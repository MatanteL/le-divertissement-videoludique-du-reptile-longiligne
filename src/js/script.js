
document.addEventListener("DOMContentLoaded", function(event) {

//Jeu

class Jeu{

    constructor(_idSVG, _idPointage){

        this.s = Snap(_idSVG);
        this.sortiePointage = document.querySelector(_idPointage);
        this.grandeurCarre = 20;
        this.grandeurGrille = 13;

    }



    nouvellePartie(){

        this.affichagePointage(1);

        this.pomme = new Pomme(this);

        this.serpent = new Serpent(this);

    }

    finPartie(){

        if(this.pomme !== undefined){


this.pomme.supprimePomme();
this.pomme = undefined;
        }

        if(this.serpent !== undefined){
            this.serpent.supprimerSerpent();
            this.serpent = undefined;
        }
    }

    affichagePointage(_lePointage){
        this.sortiePointage.innerHTML = _lePointage;
    }

}



// Serpent

    class Serpent{

        constructor(_leJeu){

            this.leJeu = _leJeu;

            this.currentX = -1;
            this.currentY = 0;

            this.nextMoveX = 1;
            this.nextMoveY = 0;

            this.serpentLongueur = 1;
            this.tblCarreSerpent = [];


            this.touche = false;

            this.vitesse = 250;
            this.timing = setInterval(this.controleSerpent.bind(this), this.vitesse);

            document.addEventListener("keydown", this.verifTouche.bind(this));


                }

        verifTouche(_evt){

            var evt = _evt;

            console.log(evt.keyCode);

            this.deplacement(evt.keyCode);

        }

        deplacement(_dirCode){
switch(_dirCode){
    case 37:
        this.nextMoveX = -1;
        this.nextMoveY = 0;
break;
    case 38:
        this.nextMoveX = 0;
        this.nextMoveY = -1;
        break;
    case 39:
        this.nextMoveX = 1;
        this.nextMoveY = 0;
        break;
    case 40:
        this.nextMoveX = 0;
        this.nextMoveY = 1;
        break;

}

        }

        controleSerpent(){

var nextX = this.currentX + this.nextMoveX;
var nextY = this.currentY + this.nextMoveY;

this.tblCarreSerpent.forEach(function(element){
    if(nextX === element[1] && nextY === element[2]){
        console.log("Hey, c'est moi ça!");
        this.leJeu.finPartie();
        this.touche = true;
    }
}.bind(this));

if(nextY < 0 || nextX < 0 || nextY > this.leJeu.grandeurGrille-1 || nextX > this.leJeu.grandeurGrille-1 ){

    this.leJeu.finPartie();
    this.touche = true;
}
if(this.touche == true){

    if(this.currentX === this.leJeu.pomme.pomme[1] && this.currentY === this.leJeu.pomme.pomme[2]){
        this.serpentLongueur ++;

        this.leJeu.affichagePointage(this.serpentLongueur);

        this.leJeu.pomme.supprimePomme();
        this.leJeu.pomme.ajoutePomme();

    }
this.dessineCarre(nextX, nextY);
            this.currentX = nextX;
            this.currentY = nextY;
}
        }

        dessineCarre(x, y){
        var unCarre = [this.leJeu.s.rect(x * this.leJeu.grandeurCarre, y * this.leJeu.grandeurCarre, this.leJeu.grandeurCarre, this.leJeu.grandeurCarre), x, y];

        this.tblCarreSerpent.push(unCarre);

        if(this.tblCarreSerpent.length > this.serpentLongueur){
            this.tblCarreSerpent[0][0].remove();
            this.tblCarreSerpent.shift();
        }
        }

        supprimerSerpent(){
        clearInterval(this.timing);
        while(this.tblCarreSerpent.length > 0)
            this.tblCarreSerpent[0][0].remove();
        this.tblCarreSerpent.shift();
        }

    }



// Pomme

    class Pomme{

        constructor(_leJeu){

            this.leJeu = _leJeu;

            this.pomme = [];

            this.ajoutePomme();


        }

        ajoutePomme(){


            var posX = Math.floor(Math.random() * this.leJeu.grandeurGrille + 1);
            var posY = Math.floor(Math.random() * this.leJeu.grandeurGrille + 1);

            console.log(posX, posY);

            this.pomme = [this.leJeu.s.rect(posX * this.leJeu.grandeurCarre, posY * this.leJeu.grandeurCarre, this.leJeu.grandeurCarre, this.leJeu.grandeurCarre).attr({fill:'red'}), posX, posY];


        }

        supprimePomme(){
            this.pomme[0].remove;
        }

    }

    var unePartie = new Jeu("#jeu", "#pointage");

var btnJouer = document.querySelector("#btnJouer");
btnJouer.addEventListener("click", nouveauParti);

function nouveauParti(){
    unePartie.nouvellePartie();
}
});